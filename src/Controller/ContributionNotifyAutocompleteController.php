<?php

namespace Drupal\contribution_notify\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Contribution Notify Autocomplete Controller.
 */
class ContributionNotifyAutocompleteController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a Contribution Notify Autocomplete Controller object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Returns account autocomplete matches.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function autocomplete(Request $request) {
    $q = $request->query->get('q');

    $response = $this->httpClient->get('https://www.drupal.org/index.php?q=admin/views/ajax/autocomplete/user/' . urlencode($q));
    $data = Json::decode($response->getBody());
    $matches = [];
    foreach ($data as $value) {
      $matches[] = ['value' => $value, 'label' => $value];
    }
    return new JsonResponse($matches);
  }

}
