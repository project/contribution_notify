<?php

namespace Drupal\contribution_notify\Form;

use Drupal\contribution_notify\ContributionNotifyManagerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure contribute settings for this site.
 */
class ContributionNotifySettingsForm extends ConfigFormBase {

  /**
   * The contribution notify manager.
   *
   * @var \Drupal\contribution_notify\ContributionNotifyManagerInterface
   */
  protected $contributeManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contribution_notify_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['contribution_notify.settings'];
  }

  /**
   * Constructs a ContributeSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\contribution_notify\ContributionNotifyManagerInterface $contribute_manager
   *   The contribution notify manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ContributionNotifyManagerInterface $contribute_manager) {
    parent::__construct($config_factory);
    $this->contributeManager = $contribute_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('contribution_notify.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('contribute.settings');
    $form['#attributes'] = ['novalidate' => TRUE];
    $form['account'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Drupal.org Account'),
      '#states' => [
        'visible' => [
          ':input[name="disable"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['account']['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal.org user name'),
      '#description' => $this->t('Enter your user name. <a href=":href">Create new user account</a>', [':href' => 'https://register.drupal.org/user/register']),
      '#default_value' => ($config->get('account_type') === 'user') ? $config->get('account_id') : '',
      '#autocomplete_route_name' => 'contribution_notify.autocomplete',
    ];

    $form['disable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Do not display contribution information"),
      '#default_value' => !$config->get('status'),
      '#return_value' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear'),
      '#attributes' => [
        'class' => ['button--danger'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ((string) $form_state->getValue('op') === (string) $this->t('Clear') || $form_state->getValue('disable')) {
      return;
    }

    $account_type = $form_state->getValue('account_type');
    $account_id = $form_state->getValue($account_type . '_id');

    $this->contributeManager->setAccountType($account_type);
    $this->contributeManager->setAccountId($account_id);

    $account = $this->contributeManager->getAccount();
    if (!$account['status']) {
      $t_args = [
        '@name' => ($account_type === 'individual') ? $this->t('Drupal.org user name') : $this->t('Drupal.org organization name'),
      ];
      $form_state->setErrorByName($account_type . '_id', $this->t('Invalid @name.', $t_args));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ((string) $form_state->getValue('op') === (string) $this->t('Clear')) {
      $status = TRUE;
      $account_type = NULL;
      $account_id = NULL;
      $this->messenger()->addMessage($this->t('Community information has been cleared.'));
    }
    elseif ($form_state->getValue('disable')) {
      $status = FALSE;
      $account_type = NULL;
      $account_id = NULL;
      $this->messenger()->addMessage($this->t('Community information has been disabled.'));
    }
    else {
      $status = TRUE;
      $account_type = $form_state->getValue('account_type');
      $account_id = $form_state->getValue($account_type . '_id');
      $this->messenger()->addMessage($this->t('Your community information has been saved.'));
    }

    // Always clear cached information.
    Cache::invalidateTags(['contribute']);

    $this->config('contribute.settings')
      ->set('status', $status)
      ->set('account_type', $account_type)
      ->set('account_id', $account_id)
      ->save();
  }

}
